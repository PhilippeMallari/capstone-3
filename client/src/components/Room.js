import React, { useEffect, useState } from "react";
import {
	Card,
	Container,
	Columns,
	Section,
	Button,
	Heading,
	Box,
	Image,
	Dropdown
} from "react-bulma-components";
import { graphql } from "react-apollo";
import { flowRight as compose } from "lodash";
import { Link, Redirect } from "react-router-dom";
import Swal from "sweetalert2";
import DemoCarousel from "./DemoCarousel";

import DatePicker from "react-datepicker";
import { toBase64, nodeServer } from "../function.js";

import "react-datepicker/dist/react-datepicker.css";

//queries
import {
	getRoomsQuery,
	getPaymentModes,
	getRoomQuery
} from "../queries/queries.js";

//mutation
import {
	updateRoomMutation,
	createTransactionMutation
} from "../queries/mutations.js";

const Room = props => {
	// console.log(props);

	const [payment_mode_id, setPaymentModeId] = useState("");
	const [user_id, setUserId] = useState(localStorage.getItem("user_id"));
	const [isBook, setIsBook] = useState(false);
	const [room_id, setRoom_id] = useState("");

	const [start_date, setStartDate] = useState(new Date("2019/12/1"));
	const [end_date, setEndDate] = useState(new Date("2019/12/2"));

	const roomData = props.getRoomsQuery.getRooms
		? props.getRoomsQuery.getRooms
		: [];

	const paymentModes = props.getPaymentModes.getPaymentModes
		? props.getPaymentModes.getPaymentModes
		: [];

	// const startDateChangeHandler = e => {
	// 	setStartDate(e.target.value);
	// };
	// const endDateChangeHandler = e => {
	// 	setEndDate(e.target.value);
	// };

	const setPaymentModeIdHandler = e => {
		setPaymentModeId(e.target.value);
		console.log(e.target.value);
	};

	const formSubmitHandler = e => {
		e.preventDefault();
		console.log(e.target.price_id.value);
		// console.log(start_date);
		// console.log(end_date);

		console.log("Submitting now to transactions page");
		if (localStorage.getItem("role") === "User") {
			setRoom_id(e.target.room_id.value);

			let newTransaction = {
				room_id: e.target.room_id.value,
				user_id: user_id,
				payment_mode_id: payment_mode_id,
				total_price: e.target.price_id.value,
				check_in: start_date,
				check_out: end_date,
				status: "pending"
			};
			console.log(newTransaction);
			props
				.createTransactionMutation({
					variables: newTransaction
				})
				.then(res => {
					setIsBook(true);
					console.log(res);
					let resUsername =
						res.data.createTransaction.user_id[0].username;
					console.log(resUsername);
					console.log(localStorage.username);

					localStorage.setItem("userWhoBooked", resUsername);

					console.log(localStorage);
				});
		} else {
			Swal.fire("Warning", "Log in first", "warning");
		}
	};

	if (isBook) {
		return <Redirect to={"/transaction/" + room_id} />;
	}

	// console.log(props);
	const paymentOptions = () => {
		let paymentData = props.getPaymentModes;
		if (paymentData.loading) {
			return <option>Loading payment modes...</option>;
		} else {
			return paymentModes.map(paymentMode => {
				return (
					<option value={paymentMode.id}>{paymentMode.name}</option>
				);
			});
		}
	};

	return (
		<Section>
			<DemoCarousel />
			{roomData.map(room => {
				if (room.status === "available") {
					return (
						<div>
							<Container fluid>
								<Box>
									<Columns className="is-vcentered">
										<Columns.Column size={4}>
											<div style={{ width: 460 }}>
												{/*<Image
													rounded={("rounded", false)}
													src="http://bulma.io/images/placeholders/640x480.png"
													size="3by2"
												/>*/}
												<img
													src={
														nodeServer() +
														room.imageLocation
													}
													size="3by2"
													rounded={("rounded", false)}
												/>
											</div>
										</Columns.Column>

										<Columns.Column size={8}>
											<Card>
												<Card.Header>
													<Card.Header.Title
														style={{
															fontSize: "30px"
														}}
													>
														{room.name}
													</Card.Header.Title>
												</Card.Header>
												<Card.Content>
													<label className="label">
														Description:
													</label>
													<p>{room.description}</p>

													<label className="label">
														Room Type:
													</label>
													<p>{room.room_type}</p>

													<label className="label">
														Max Persons:
													</label>
													<p>{room.persons}</p>

													<label className="label">
														Bed Size:
													</label>
													<p>{room.bed_size}</p>

													<label className="label">
														Status
													</label>
													<p>{room.status}</p>
												</Card.Content>
											</Card>
										</Columns.Column>
									</Columns>
								</Box>
							</Container>

							<Container fluid>
								<Box>
									<Columns className="is-vcentered">
										<Columns.Column
											size={4}
											style={{ fontSize: "30px" }}
										>
											<strong>{room.name}</strong>
										</Columns.Column>
										<Columns.Column
											size={4}
											style={{ fontSize: "30px" }}
										>
											<strong>
												Room Price: {room.price} per
												night{" "}
											</strong>

											<label className="label">
												Payment Mode:
											</label>
											<div className="control">
												<div className="select">
													<select
														onChange={
															setPaymentModeIdHandler
														}
													>
														<option
															disable
															selected
														>
															Select Payment
															Mode..
														</option>
														{paymentOptions()}
													</select>
												</div>
											</div>
										</Columns.Column>
										<Columns.Column size={4}>
											<form onSubmit={formSubmitHandler}>
												<input
													type="hidden"
													value={room.id}
													id="room_id"
												/>
												<input
													type="hidden"
													value={room.price}
													id="price_id"
												/>
												<div className="field">
													<label className="label">
														Start Date:
													</label>
													<DatePicker
														selected={start_date}
														onChange={date =>
															setStartDate(date)
														}
														selectsStart
														startDate={start_date}
														endDate={end_date}
														value={start_date}
														dateFormat="MMMM d, yyyy"
														className="input"
													/>
												</div>
												<div className="field">
													<label className="label">
														End Date:
													</label>
													<DatePicker
														selected={end_date}
														onChange={date =>
															setEndDate(date)
														}
														selectsEnd
														startDate={start_date}
														endDate={end_date}
														minDate={start_date}
														value={end_date}
														dateFormat="MMMM d, yyyy"
														className="input"
													/>
												</div>
												{/*<input
													type="date"
													className="input"
													value={start_date}
													onChange={
														startDateChangeHandler
													}
												/>*/}

												{/*<input
													type="date"
													className="input"
													value={end_date}
													onChange={
														endDateChangeHandler
													}
												/>*/}
												{localStorage.getItem(
													"role"
												) === "Admin" ? (
													""
												) : (
													<Button fullwidth>
														Book Now
													</Button>
												)}
											</form>
										</Columns.Column>
									</Columns>
								</Box>
							</Container>
							<hr />
						</div>
					);
				}
			})}
		</Section>
	);
};

export default compose(
	graphql(getRoomsQuery, { name: "getRoomsQuery" }),
	graphql(getPaymentModes, { name: "getPaymentModes" }),
	graphql(updateRoomMutation, { name: "updateRoomMutation" }),
	graphql(createTransactionMutation, { name: "createTransactionMutation" }),
	graphql(getRoomQuery, {
		options: props => {
			return {
				variables: {
					id: props.match.params.id
				}
			};
		},
		name: "getRoomQuery"
	})
)(Room);
