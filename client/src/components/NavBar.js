import React, { useState, useEffect } from "react";
import { Navbar } from "react-bulma-components";
import { Link, Redirect } from "react-router-dom";
import Swal from "sweetalert2";

const NavBar = props => {
	const [username, setUsername] = useState(localStorage.getItem("username"));

	const logoutHandler = e => {
		console.log("You clicked the logout button");
		localStorage.clear();

		Swal.fire({
			icon: "success",
			title: "Successfully Logged Out!",
			type: "success"
		});

		return <Redirect to="/login" />;
	};

	const show = props => {
		if (!localStorage.username) {
			return (
				<Navbar.Menu>
					<Navbar.Container position="end">
						<Link className="navbar-item" to="/">
							<strong>Register</strong>
						</Link>
						<Link className="navbar-item" to="/login">
							<strong>Login</strong>
						</Link>
						<Link className="navbar-item" to="/book">
							<strong>Book Now</strong>
						</Link>

						<p className="navbar-item">
							<strong>
								Welcome!{" "}
								{localStorage.username
									? localStorage.username
									: "Guest"}
							</strong>
						</p>
					</Navbar.Container>
				</Navbar.Menu>
			);
		} else {
			return (
				<Navbar.Menu>
					<Navbar.Container position="end">
						<Link className="navbar-item" to="/book">
							<strong>Book Now</strong>
						</Link>
						{localStorage.getItem("role") === "Admin" ? (
							<Navbar.Item dropdown hoverable href="#">
								<Navbar.Link arrowless={false}>
									<strong>Admin</strong>
								</Navbar.Link>

								<Navbar.Dropdown>
									<Link
										className="navbar-item"
										to="/createroom"
									>
										<strong>Create Room</strong>
									</Link>
									<Link
										className="navbar-item"
										to="/transactions"
									>
										<strong>Transactions</strong>
									</Link>
								</Navbar.Dropdown>
							</Navbar.Item>
						) : (
							""
						)}

						<Navbar.Item dropdown hoverable href="#">
							<Navbar.Link arrowless={false}>
								<strong>
									Welcome!{" "}
									{localStorage.username
										? localStorage.username
										: "Guest"}
								</strong>
							</Navbar.Link>
							<Navbar.Dropdown>
								<Link className="navbar-item" to="/logout">
									<strong>Logout</strong>
								</Link>
							</Navbar.Dropdown>
						</Navbar.Item>
					</Navbar.Container>
				</Navbar.Menu>
			);
		}
	};

	return (
		<Navbar id="navbar">
			<Navbar.Brand>
				<Link to="/book" className="navbar-item">
					<img
						src="/images/montalban_image.png"
						className="montalban-logo"
					/>
				</Link>
			</Navbar.Brand>
			{show()}
		</Navbar>
	);
};

export default NavBar;
