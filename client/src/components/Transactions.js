import React, { useEffect, useState } from "react";
import {
	Section,
	Container,
	Columns,
	Card,
	Button
} from "react-bulma-components";
import { graphql } from "react-apollo";
import { flowRight as compose } from "lodash";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";

// queries
import { getTransactionsQuery } from "../queries/queries.js";

const Transactions = () => {
	return (
		<div>
			<Section>
				<Container fluid>
					<Columns className="is-vcentered">
						<Columns.Column size={9} style={{ margin: "0px auto" }}>
							<Card>
								<Card.Header>
									<Card.Header.Title>
										Transactions
									</Card.Header.Title>
								</Card.Header>

								<Card.Content>
									<div className="table-container">
										<table className="table is-fullwidth is-bordered">
											<thead>
												<tr>
													<th>Room Name</th>
													<th>Description</th>
													<th>Room Type</th>
													<th>Image</th>
													<th>Max Persons</th>
													<th>Price</th>
													<th>Bed Size</th>
													<th>Status</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
													<td>
														<Link>
															<Button
																fullwidth
																id="room-update-button"
															>
																Approve
															</Button>
														</Link>
														<Button
															fullwidth
															className="room-delete-button"
														>
															Deny
														</Button>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</Card.Content>
							</Card>
						</Columns.Column>
					</Columns>
				</Container>
			</Section>
		</div>
	);
};

export default compose(
	graphql(getTransactionsQuery, { name: "getTransactionsQuery" })
)(Transactions);
