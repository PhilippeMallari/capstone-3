import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import {
	Section,
	Heading,
	Card,
	Columns,
	Container,
	Button
} from "react-bulma-components";
import { flowRight as compose } from "lodash";
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
import { format } from "date-fns";

// queries
import {
	getRoomsQuery,
	getRoomQuery,
	getTransactionsQuery
} from "../queries/queries.js";

const Transaction = props => {
	console.log(props);
	// console.log(localStorage);

	const transactionData = props.getTransactionsQuery.getTransactions
		? props.getTransactionsQuery.getTransactions
		: [];

	console.log(transactionData);
	// localStorage.username
	// localStorage.userWhoBooked

	const formSubmitHandler = e => {
		e.preventDefault();
		Swal.fire({
			icon: "success",
			title:
				"Successfully Booked! please wait for a text regarding your booking. Thank you!",
			type: "success",
			showCancelButton: false,
			showConfirmButton: false
		});
	};

	return (
		<div>
			<Section>
				<Columns>
					<Columns.Column size={1}></Columns.Column>
					<Heading>Your Details:</Heading>
				</Columns>
			</Section>
			<Section className="date-section">
				<Columns>
					<Columns.Column size={1}></Columns.Column>
					<Columns.Column size={6} className="is-vcentered">
						{transactionData.map(transaction => {
							return (
								<div className="field">
									<label className="label">Check In:</label>{" "}
									<p className="p-font-size">
										{transaction.check_in}
									</p>
									<label className="label">Check Out: </label>{" "}
									<p className="p-font-size">
										{transaction.check_out}
									</p>
								</div>
							);
						})}
					</Columns.Column>
				</Columns>
			</Section>
			<Section>
				<Columns>
					<Columns.Column size={1}></Columns.Column>
					<Heading>Rooms Booking</Heading>
				</Columns>
			</Section>
			<Section className="date-section">
				<Columns>
					<Columns.Column size={1}></Columns.Column>
					<Columns.Column size={4} className="is-vcentered">
						{transactionData.map(transaction => {
							console.log(transaction.room_id[0].name);
							return (
								<div className="field">
									<label className="label">Room Name:</label>
									<p className="p-font-size">
										{transaction.room_id[0].name}
									</p>
									<label className="label">
										Room Description:{" "}
									</label>
									<p className="p-font-size">
										{transaction.room_id[0].description}
									</p>
									<label className="label">Room Type: </label>
									<p className="p-font-size">
										{transaction.room_id[0].room_type}
									</p>

									<label className="label">Bed Size: </label>
									<p className="p-font-size">
										{transaction.room_id[0].bed_size}
									</p>
								</div>
							);
						})}
					</Columns.Column>

					<Columns.Column size={3} className="is-vcentered">
						{transactionData.map(transaction => {
							return (
								<p className="p-font-size">
									<label className="label">
										Max Persons:{" "}
									</label>
									{transaction.room_id[0].persons}
								</p>
							);
						})}
					</Columns.Column>

					<Columns.Column size={3} className="is-vcentered">
						{transactionData.map(transaction => {
							return (
								<p className="p-font-size">
									<label className="label">
										Total Price:{" "}
									</label>
									{transaction.total_price}
								</p>
							);
						})}
					</Columns.Column>
				</Columns>
			</Section>
			<Section>
				<Columns>
					<Columns.Column size={1}></Columns.Column>
					<Heading>Guest and Payment Details</Heading>
				</Columns>
			</Section>
			<Section className="date-section">
				<Columns>
					<Columns.Column size={1}></Columns.Column>
					<Columns.Column size={4} className="is-vcentered">
						{transactionData.map(transaction => {
							return (
								<div className="field">
									<label className="label">
										Payment Mode:
									</label>
									<p className="p-font-size">
										{transaction.payment_mode_id[0].name}
									</p>
									<label className="label">Username: </label>
									<p className="p-font-size">
										{transaction.user_id[0].username}
									</p>
									<label className="label">Email: </label>
									<p className="p-font-size">
										{transaction.user_id[0].email}
									</p>
								</div>
							);
						})}
						<form onSubmit={formSubmitHandler}>
							<Button type="submit" style={{ marginTop: "10px" }}>
								BOOK NOW
							</Button>
						</form>
					</Columns.Column>
				</Columns>
			</Section>
		</div>
	);
};

export default compose(
	graphql(getRoomsQuery, { name: "getRoomsQuery" }),
	graphql(getTransactionsQuery, {
		options: () => {
			return {
				variables: {
					user_id: localStorage.getItem("user_id")
				}
			};
		},
		name: "getTransactionsQuery"
	}),
	graphql(getRoomQuery, {
		options: props => {
			return {
				variables: {
					id: props.match.params.id
				}
			};
		},
		name: "getRoomQuery"
	})
)(Transaction);
