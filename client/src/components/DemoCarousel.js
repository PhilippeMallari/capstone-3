import React, { Component } from "react";
import {
	Card,
	Container,
	Columns,
	Section,
	Button,
	Heading,
	Box,
	Image,
	Dropdown,
	Modal
} from "react-bulma-components";
import ReactDOM from "react-dom";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from "react-responsive-carousel";
import styles from "react-responsive-carousel/lib/styles/carousel.min.css";

const DemoCarousel = () => {
	return (
		<Carousel
			infiniteLoop={true}
			autoPlay={true}
			interval={4000}
			swipeable={true}
			transitionTime={1000}
			showStatus={true}
			showThumbs={false}
			className="carousel-style"
		>
			<div>
				<img src="/images/banner.jpg" className="img-carousel" />

				<Container>
					<p className="legend">Book now!</p>
				</Container>
			</div>
			<div>
				<img src="images/superior.jpg" className="img-carousel" />

				<Container>
					<p className="legend">
						"Superior room : 5 persons : P3,000/12 hours"
					</p>
				</Container>
			</div>
			<div>
				<img src="images/deluxe.jpg" className="img-carousel" />

				<Container>
					<p className="legend">
						"Deluxe room : 4 persons : P2,500/12 hours"
					</p>
				</Container>
			</div>
		</Carousel>
	);
};

export default DemoCarousel;

// Don't forget to include the css in your page

// Using webpack
// import styles from 'react-responsive-carousel/lib/styles/carousel.min.css';

// Using html tag:
// <link rel="stylesheet" href="<NODE_MODULES_FOLDER>/react-responsive-carousel/lib/styles/carousel.min.css"/>
