import React from "react";

import {
	Card,
	Container,
	Content,
	Columns,
	Section,
	Button,
	Heading,
	Box,
	Image,
	Dropdown,
	Modal,
	Footer,
	Hero
} from "react-bulma-components";

const DemoFooter = () => {
	return (
		<Footer
			style={{
				backgroundColor: "#ffe500"
			}}
		>
			<Container>
				<Content style={{ textAlign: "center" }}>
					<strong>
						<p>
							No copyright infringement intended. All images and
							description belongs to Montalban Waterpark and
							Garden Resort only.
						</p>
					</strong>
				</Content>
			</Container>
		</Footer>
	);
};

export default DemoFooter;
