import React, { useState, useEffect } from "react";
import {
	Container,
	Columns,
	Section,
	Button,
	Card
} from "react-bulma-components";
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
import { Link, Redirect } from "react-router-dom";

//mutations
import { loginUserMutation } from "../queries/mutations";

const LoginUser = props => {
	console.log(props);
	const [username, setUsername] = useState("");
	const [password, setPassword] = useState("");
	const [logInSuccess, setLogInSuccess] = useState(false);

	useEffect(() => {
		console.log("Value of " + username);
		console.log("Value of " + password);
	});

	const userNameChangeHandler = e => {
		setUsername(e.target.value);
	};

	const passwordChangeHandler = e => {
		setPassword(e.target.value);
	};

	const submitFormHandler = e => {
		e.preventDefault();
		console.log("Submitting the login form");
		props
			.loginUserMutation({
				variables: {
					username: username,
					password: password
				}
			})
			.then(res => {
				console.log(res);
				let data = res.data.loginUser;
				console.log(data);

				if (data === null) {
					Swal.fire({
						title: "Login Failed",
						text: "wrong credentials",
						icon: "error"
					});
				} else {
					Swal.fire({
						icon: "success",
						title: "Successfully Logged In!",
						type: "success"
					});
					localStorage.setItem("username", data.username);
					localStorage.setItem("user_id", data.id);
					localStorage.setItem("role", data.role);
					props.updateSession();
					setLogInSuccess(true);

					console.log(localStorage);
				}
			});
	};

	if (!logInSuccess) {
		console.log("something went wrong");
	} else {
		if (localStorage.getItem("role") === "Admin") {
			return <Redirect to="/createroom" />;
		}
		return <Redirect to="/book" />;
	}

	return (
		<div>
			<Section style={{ height: "80vh" }}>
				<Container>
					<Columns>
						<Columns.Column size={6} style={{ margin: "0px auto" }}>
							<Card>
								<Card.Header>
									<Card.Header.Title>Login</Card.Header.Title>
								</Card.Header>
								<Card.Content>
									<form onSubmit={submitFormHandler}>
										<div className="field">
											<label
												className="label"
												htmlFor="uName"
											>
												Username
											</label>
											<input
												id="uName"
												className="input"
												placeholder="Username.."
												type="text"
												value={username}
												onChange={userNameChangeHandler}
											/>
										</div>
										{/*end of field*/}

										<div className="field">
											<label
												className="label"
												htmlFor="password"
											>
												Password
											</label>
											<input
												id="password"
												className="input"
												placeholder="Password.."
												type="password"
												value={password}
												onChange={passwordChangeHandler}
											/>
										</div>
										<Button type="submit" fullwidth>
											{" "}
											Login
										</Button>
									</form>
								</Card.Content>
							</Card>
						</Columns.Column>
					</Columns>
				</Container>
			</Section>
		</div>
	);
};

export default graphql(loginUserMutation, { name: "loginUserMutation" })(
	LoginUser
);
