import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import {
	Section,
	Heading,
	Card,
	Columns,
	Container,
	Button
} from "react-bulma-components";
import { flowRight as compose } from "lodash";
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
import { toBase64, nodeServer } from "../function.js";

// queries
import { getRoomsQuery, getRoomQuery } from "../queries/queries.js";

// mutations
import { updateRoomMutation } from "../queries/mutations";

const UpdateRoom = props => {
	console.log(props);

	const room = props.getRoomQuery.getRoom ? props.getRoomQuery.getRoom : {};
	console.log(room);

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [room_type, setRoomType] = useState("");

	const [persons, setMaxPersons] = useState("");
	const [price, setPrice] = useState("");
	const [bed_size, setBedSize] = useState("");
	const [status, setStatus] = useState("");
	const [imagePath, setImagePath] = useState("");
	const fileRef = React.createRef();

	useEffect(() => {
		console.log("The value of name" + name);
		console.log("The value of description" + description);
		console.log("The value of room_type" + room_type);

		console.log("The value of persons" + persons);
		console.log("The value of price" + price);
		console.log("The value of bed_size" + bed_size);
	});

	if (!props.getRoomQuery.loading) {
		const setDefaultValues = () => {
			console.log(room);
			setName(room.name);
			setDescription(room.description);
			setRoomType(room.room_type);
			setImagePath(room.imageLocation);
			setMaxPersons(room.persons);
			setPrice(room.price);
			setBedSize(room.bed_size);
			setStatus(room.status);
		};

		if (name === "") {
			setDefaultValues();
			console.log("The value of set default" + name);
		}
	}

	const roomNameChangeHandler = e => {
		setName(e.target.value);
	};
	const descriptionChangeHandler = e => {
		setDescription(e.target.value);
	};
	const roomTypeChangeHandler = e => {
		setRoomType(e.target.value);
	};

	const maxPersonsChangeHandler = e => {
		setMaxPersons(e.target.value);
	};
	const priceChangeHandler = e => {
		setPrice(e.target.value);
	};
	const bedSizeChangeHandler = e => {
		setBedSize(e.target.value);
	};
	const statusChangeHandler = e => {
		setStatus(e.target.value);
	};

	const formSubmitHandler = e => {
		e.preventDefault();

		let updatedRoom = {
			id: props.match.params.id,
			name: name,
			description: description,
			room_type: room_type,
			imageLocation: imagePath,
			persons: persons,
			price: price,
			bed_size: bed_size,
			status: status
		};

		console.log(updatedRoom);
		props
			.updateRoomMutation({
				variables: updatedRoom
			})
			.then(res => {
				console.log(res);
			});

		if (
			name === "" ||
			description === "" ||
			room_type === "" ||
			imagePath === "" ||
			persons === "" ||
			price === "" ||
			bed_size === "" ||
			status === ""
		) {
			Swal.fire({
				icon: "error",
				title: "Please complete the form!",
				type: "success",
				showCancelButton: false,
				showConfirmButton: true
			});
		} else {
			Swal.fire({
				icon: "success",
				title: "Room Updated",
				text: "Room has been updated!",
				type: "success",

				// first approach
				html:
					'<a href="/createroom" class="button is-success">Go back to rooms </a>',
				showCancelButton: false,
				showConfirmButton: false

				// or
				// confirmButtonText:
				// '<a href="/" class="has-text-white">Go back to members </a>'
			});
		}
	};

	const imagePathHandler = e => {
		// console.log(fileRef.current.files[0]);
		toBase64(fileRef.current.files[0]).then(encodedFile => {
			console.log(encodedFile);
			setImagePath(encodedFile);
		});
	};

	return (
		<div>
			<Section>
				<Container>
					<Columns className="is-vcentered">
						<Columns.Column size={6} style={{ margin: "0px auto" }}>
							<Card>
								<Card.Header>
									<Card.Header.Title>
										Update Room
									</Card.Header.Title>
								</Card.Header>
								<Card.Content>
									<form onSubmit={formSubmitHandler}>
										<div className="field">
											<label
												className="label"
												htmlFor="roomName"
											>
												Name
											</label>
											<input
												id="roomName"
												className="input"
												type="text"
												onChange={roomNameChangeHandler}
												value={name}
											/>
										</div>
										{/* end of field*/}

										<div className="field">
											<label
												className="label"
												htmlFor="description"
											>
												Description
											</label>
											<input
												id="description"
												className="input"
												type="text"
												onChange={
													descriptionChangeHandler
												}
												value={description}
											/>
										</div>
										{/* end of field*/}

										<div className="field">
											<label
												className="label"
												htmlFor="roomType"
											>
												Room Type
											</label>
											<input
												id="roomType"
												className="input"
												type="text"
												onChange={roomTypeChangeHandler}
												value={room_type}
											/>
										</div>
										{/* end of field*/}

										<div className="field">
											<label
												className="label"
												htmlFor="image"
											>
												Image
											</label>
											<input
												id="image"
												className="input"
												type="file"
												accept="image/png"
												onChange={imagePathHandler}
												ref={fileRef}
											/>
										</div>
										{/* end of field*/}

										<div className="field">
											<label
												className="label"
												htmlFor="persons"
											>
												Max Persons
											</label>
											<input
												id="persons"
												className="input"
												type="text"
												onChange={
													maxPersonsChangeHandler
												}
												value={persons}
											/>
										</div>
										{/* end of field*/}

										<div className="field">
											<label
												className="label"
												htmlFor="price"
											>
												Price
											</label>
											<input
												id="price"
												className="input"
												type="number"
												min="1"
												onChange={priceChangeHandler}
												value={price}
											/>
										</div>
										{/* end of field*/}

										<div className="field">
											<label
												className="label"
												htmlFor="bedSize"
											>
												Bed Size
											</label>
											<input
												id="bedSize"
												className="input"
												type="text"
												onChange={bedSizeChangeHandler}
												value={bed_size}
											/>
										</div>
										{/* end of field*/}

										<div className="field">
											<label
												className="label"
												htmlFor="status"
											>
												Status
											</label>
											<input
												id="status"
												className="input"
												type="text"
												onChange={statusChangeHandler}
												value={status}
											/>
										</div>
										{/* end of field*/}

										<Button type="submit" fullwidth>
											Update Room
										</Button>

										<Link to="/createroom">
											<Button
												type="button"
												className="update-cancel-button"
												fullwidth
											>
												Cancel
											</Button>
										</Link>
									</form>
								</Card.Content>
							</Card>
						</Columns.Column>
					</Columns>
				</Container>
			</Section>
		</div>
	);
};

export default compose(
	graphql(getRoomsQuery, { name: "getRoomsQuery" }),
	graphql(updateRoomMutation, { name: "updateRoomMutation" }),
	graphql(getRoomQuery, {
		options: props => {
			return {
				variables: {
					id: props.match.params.id
				}
			};
		},
		name: "getRoomQuery"
	})
)(UpdateRoom);
