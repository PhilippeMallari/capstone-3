import React, { useEffect, useState } from "react";
import {
	Container,
	Columns,
	Section,
	Button,
	Card
} from "react-bulma-components";
import { graphql } from "react-apollo";
import { flowRight as compose } from "lodash";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";

//queries
import { getUsersQuery } from "../queries/queries";

//mutations
import { createUserMutation } from "../queries/mutations";

const RegisterUser = props => {
	console.log(props);

	// Hooks
	const [username, setUsername] = useState("");
	const [email, setEmail] = useState("");
	const [contact_number, setContactNumber] = useState("");
	const [password, setPassword] = useState("");

	useEffect(() => {
		console.log("value of username" + username);
		console.log("value of email" + email);
		console.log("value of contact number" + contact_number);
		console.log("value of password" + password);
	});

	const userNameChangeHandler = e => {
		setUsername(e.target.value);
	};

	const emailChangeHandler = e => {
		setEmail(e.target.value);
	};

	const contactNumberChangeHandler = e => {
		setContactNumber(e.target.value);
	};

	const passwordChangeHandler = e => {
		setPassword(e.target.value);
	};

	const addUser = e => {
		e.preventDefault();

		let newUser = {
			username: username,
			email: email,
			contact_number: contact_number,
			password: password
		};

		props
			.createUserMutation({
				variables: newUser,
				refetchQueries: [
					{
						query: getUsersQuery
					}
				]
			})
			.then(res => {
				console.log(res);
				Swal.fire({
					icon: "success",
					title: "Successfully Registered!",
					type: "success",
					html:
						'<a href="/login" class="button is-success">Login </a>',
					showCancelButton: false,
					showConfirmButton: false
				});
			})
			.catch(error => {
				Swal.fire({
					icon: "error",
					title: "Username already taken!",
					type: "success",
					showCancelButton: false,
					showConfirmButton: true
				});
			});

		if (
			username === "" ||
			email === "" ||
			contact_number === "" ||
			password === ""
		) {
			Swal.fire({
				icon: "error",
				title: "Please complete the form!",
				type: "success",
				showCancelButton: false,
				showConfirmButton: true
			});
		}
	};

	const userData = props.getUsersQuery.getUsers
		? props.getUsersQuery.getUsers
		: [];
	console.log(userData);

	return (
		<div>
			<Section style={{ height: "80vh" }}>
				<Container>
					<Columns className="is-vcentered">
						<Columns.Column size={6} style={{ margin: "0px auto" }}>
							<Card>
								<Card.Header>
									<Card.Header.Title>
										Register
									</Card.Header.Title>
								</Card.Header>
								<Card.Content>
									<form onSubmit={addUser}>
										<div className="field">
											<label
												className="label"
												htmlFor="uName"
											>
												Username
											</label>
											<input
												id="uName"
												className="input"
												type="text"
												onChange={userNameChangeHandler}
												value={username}
												placeholder="Username.."
											/>
										</div>
										{/*end of field*/}

										<div className="field">
											<label
												className="label"
												htmlFor="email"
											>
												Email
											</label>
											<input
												id="email"
												className="input"
												type="email"
												onChange={emailChangeHandler}
												value={email}
												placeholder="Email.."
											/>
										</div>
										{/*end of field*/}

										<div className="field">
											<label
												className="label"
												htmlFor="cNumber"
											>
												Contact Number
											</label>
											+63
											<input
												id="cNumber"
												className="input"
												type="text"
												onChange={
													contactNumberChangeHandler
												}
												value={contact_number}
												placeholder="Contact Number.."
											/>
										</div>
										{/*end of field*/}

										<div className="field">
											<label
												className="label"
												htmlFor="password"
											>
												Password
											</label>
											<input
												id="password"
												className="input"
												type="password"
												onChange={passwordChangeHandler}
												value={password}
												placeholder="Password.."
											/>
										</div>
										{/*end of field*/}

										<Button type="submit" fullwidth>
											{" "}
											Register
										</Button>
									</form>
								</Card.Content>
							</Card>
						</Columns.Column>
					</Columns>
				</Container>
			</Section>
		</div>
	);
};

// getUsersQuery
// createUserMutation

export default compose(
	graphql(getUsersQuery, { name: "getUsersQuery" }),
	graphql(createUserMutation, { name: "createUserMutation" })
)(RegisterUser);
