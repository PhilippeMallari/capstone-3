import React, { useEffect, useState } from "react";
import {
	Section,
	Container,
	Columns,
	Card,
	Button
} from "react-bulma-components";
import { graphql } from "react-apollo";
import { flowRight as compose } from "lodash";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";
import { toBase64, nodeServer } from "../function.js";

// queries
import { getRoomsQuery } from "../queries/queries.js";

//mutations
import {
	createRoomMutation,
	deleteRoomMutation
} from "../queries/mutations.js";

const CreateRoom = props => {
	console.log(props);
	//hooks
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [room_type, setRoomType] = useState("");

	const [persons, setMaxPersons] = useState("");
	const [price, setPrice] = useState("");
	const [bed_size, setBedSize] = useState("");
	const [status, setStatus] = useState("");
	const [imagePath, setImagePath] = useState("");
	const fileRef = React.createRef();

	console.log(fileRef);

	useEffect(() => {
		console.log("The value of name" + name);
		console.log("The value of description" + description);
		console.log("The value of room_type" + room_type);

		console.log("The value of persons" + persons);
		console.log("The value of price" + price);
		console.log("The value of bed_size" + bed_size);
		console.log("The value of status" + status);
	});

	const roomNameChangeHandler = e => {
		setName(e.target.value);
	};
	const descriptionChangeHandler = e => {
		setDescription(e.target.value);
	};
	const roomTypeChangeHandler = e => {
		setRoomType(e.target.value);
	};
	const maxPersonsChangeHandler = e => {
		setMaxPersons(e.target.value);
	};
	const priceChangeHandler = e => {
		setPrice(e.target.value);
	};
	const bedSizeChangeHandler = e => {
		setBedSize(e.target.value);
	};

	const statusChangeHandler = e => {
		setStatus(e.target.value);
	};

	const roomData = props.getRoomsQuery.getRooms
		? props.getRoomsQuery.getRooms
		: [];

	const addRoom = e => {
		e.preventDefault();
		console.log("Creating new room..");

		let newRoom = {
			name: name,
			description: description,
			room_type: room_type,
			imageLocation: imagePath,
			persons: persons,
			price: price,
			bed_size: bed_size,
			status: status
		};
		props.createRoomMutation({
			variables: newRoom,
			refetchQueries: [
				{
					query: getRoomsQuery
				}
			]
		});

		if (
			name === "" ||
			description === "" ||
			room_type === "" ||
			imagePath === "" ||
			persons === "" ||
			price === "" ||
			bed_size === "" ||
			status === ""
		) {
			Swal.fire({
				icon: "error",
				title: "Please complete the form!",
				type: "success",
				showCancelButton: false,
				showConfirmButton: true
			});
		} else {
			Swal.fire({
				icon: "success",
				title: "Successfully Added New Room!",
				type: "success",
				showCancelButton: false,
				showConfirmButton: false
			});
		}

		setName("");
		setDescription("");
		setRoomType("");

		setMaxPersons("");
		setPrice("");
		setBedSize("");
		setStatus("");
	};

	if (props.getRoomsQuery.loading) {
		let timerInterval;
		Swal.fire({
			title: "Fetching data...",
			timer: 1000,
			timerProgressBar: true,
			onBeforeOpen: () => {
				Swal.showLoading();
			},
			onClose: () => {
				clearInterval(timerInterval);
			}
		}).then(result => {
			if (
				/* Read more about handling dismissals below */
				result.dismiss === Swal.DismissReason.timer
			) {
				console.log("I was closed by the timer"); // eslint-disable-line
			}
		});
	}

	// delete room handler
	const deleteRoomHandler = e => {
		console.log("Deleting a room..");
		console.log(e.target.id);
		let id = e.target.id;

		Swal.fire({
			title: "Are you sure you want to delete?",
			icon: "warning",
			showCancelButton: true,
			showConfirmButton: true,
			confirmButtonColor: "#3085d6",
			cancelButtonColor: "#d33",
			confirmButtonText: "Yes, delete it!",
			cancelButtonText: "No, cancel"
		}).then(result => {
			if (result.value) {
				props.deleteRoomMutation({
					variables: { id: id },
					refetchQueries: [
						{
							query: getRoomsQuery
						}
					]
				});
				Swal.fire("Deleted!", "The room has been deleted.", "success");
			}
		});
	};
	// end of delete room handler

	const imagePathHandler = e => {
		// console.log(fileRef.current.files[0]);
		toBase64(fileRef.current.files[0]).then(encodedFile => {
			console.log(encodedFile);
			setImagePath(encodedFile);
		});
	};

	return (
		<div>
			<Section>
				<Container fluid>
					<Columns className="is-vcentered">
						<Columns.Column size={3}>
							<Card>
								<Card.Header>
									<Card.Header.Title>
										Create Room
									</Card.Header.Title>
								</Card.Header>

								<Card.Content>
									<form onSubmit={addRoom}>
										<div className="field">
											<label
												className="label"
												htmlFor="roomName"
											>
												Name
											</label>
											<input
												id="roomName"
												className="input"
												type="text"
												onChange={roomNameChangeHandler}
												value={name}
											/>
										</div>
										{/* end of field*/}

										<div className="field">
											<label
												className="label"
												htmlFor="description"
											>
												Description
											</label>
											<input
												id="description"
												className="input"
												type="text"
												onChange={
													descriptionChangeHandler
												}
												value={description}
											/>
										</div>
										{/* end of field*/}

										<div className="field">
											<label
												className="label"
												htmlFor="roomType"
											>
												Room Type
											</label>
											<input
												id="roomType"
												className="input"
												type="text"
												onChange={roomTypeChangeHandler}
												value={room_type}
											/>
										</div>
										{/* end of field*/}

										<div className="field">
											<label
												className="label"
												htmlFor="image"
											>
												Add Image
											</label>
											<div className="control">
												<input
													id="image"
													className="input"
													type="file"
													accept="image/png"
													onChange={imagePathHandler}
													ref={fileRef}
												/>
											</div>
										</div>
										{/* end of field*/}

										<div className="field">
											<label
												className="label"
												htmlFor="persons"
											>
												Max Persons
											</label>
											<input
												id="persons"
												className="input"
												type="text"
												onChange={
													maxPersonsChangeHandler
												}
												value={persons}
											/>
										</div>
										{/* end of field*/}

										<div className="field">
											<label
												className="label"
												htmlFor="price"
											>
												Price
											</label>
											<input
												id="price"
												className="input"
												type="number"
												min="1"
												onChange={priceChangeHandler}
												value={price}
											/>
										</div>
										{/* end of field*/}

										<div className="field">
											<label
												className="label"
												htmlFor="bedSize"
											>
												Bed Size
											</label>
											<input
												id="bedSize"
												className="input"
												type="text"
												onChange={bedSizeChangeHandler}
												value={bed_size}
											/>
										</div>
										{/* end of field*/}

										<div className="field">
											<label
												className="label"
												htmlFor="status"
											>
												Status
											</label>
											<input
												id="status"
												className="input"
												type="text"
												onChange={statusChangeHandler}
												value={status}
											/>
										</div>
										{/* end of field*/}

										<Button type="submit" fullwidth>
											Add New Room
										</Button>
									</form>
								</Card.Content>
							</Card>
						</Columns.Column>

						<Columns.Column size={9}>
							<Card>
								<Card.Header>
									<Card.Header.Title>Rooms</Card.Header.Title>
								</Card.Header>

								<Card.Content>
									<div className="table-container">
										<table className="table is-fullwidth is-bordered">
											<thead>
												<tr>
													<th>Room Name</th>
													<th>Description</th>
													<th>Room Type</th>
													<th>Image</th>
													<th>Max Persons</th>
													<th>Price</th>
													<th>Bed Size</th>
													<th>Status</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												{roomData.map(room => {
													console.log(nodeServer());
													return (
														<tr key={room.id}>
															<td>{room.name}</td>
															<td>
																{
																	room.description
																}
															</td>
															<td>
																{room.room_type}
															</td>
															<td>
																<img
																	src={
																		nodeServer() +
																		room.imageLocation
																	}
																/>
															</td>
															<td>
																{room.persons}
															</td>
															<td>
																{room.price}
															</td>
															<td>
																{room.bed_size}
															</td>
															<td>
																{room.status}
															</td>
															<td>
																<Link
																	to={
																		"/room/update/" +
																		room.id
																	}
																>
																	<Button
																		fullwidth
																		id="room-update-button"
																	>
																		Update
																	</Button>
																</Link>
																<Button
																	id={room.id}
																	onClick={
																		deleteRoomHandler
																	}
																	fullwidth
																	className="room-delete-button"
																>
																	Delete
																</Button>
															</td>
														</tr>
													);
												})}
											</tbody>
										</table>
									</div>
								</Card.Content>
							</Card>
						</Columns.Column>
						{/* end of cols*/}
					</Columns>
				</Container>
			</Section>
		</div>
	);
};

export default compose(
	graphql(getRoomsQuery, { name: "getRoomsQuery" }),
	graphql(createRoomMutation, { name: "createRoomMutation" }),
	graphql(deleteRoomMutation, { name: "deleteRoomMutation" })
)(CreateRoom);
