import React, { useState } from "react";
import "./App.css";
import "react-bulma-components/dist/react-bulma-components.min.css";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";
import { Link, Redirect } from "react-router-dom";

// Components
import NavBar from "./components/NavBar";
import RegisterUser from "./components/RegisterUser";
import LoginUser from "./components/LoginUser";
import Room from "./components/Room";
import CreateRoom from "./components/CreateRoom";
import UpdateRoom from "./components/UpdateRoom";
import Transaction from "./components/Transaction";
import DemoFooter from "./components/DemoFooter";
import Transactions from "./components/Transactions";

//create an instance to all our GraphQL components
const client = new ApolloClient({
	uri: "https://montalban-waterpark.herokuapp.com/montalban_waterpark"
});

function App() {
	const [username, setUsername] = useState(localStorage.getItem("username"));
	const [role, setRole] = useState(localStorage.getItem("role"));
	console.log("This is the value of username: " + username);
	console.log("This is the value of role: " + role);

	const updateSession = () => {
		setUsername(localStorage.getItem("username"));
	};

	const Logout = () => {
		localStorage.clear();
		updateSession();
		return <Redirect to="/login" />;
	};

	const loggedUser = props => {
		// {spread operator} retains all the existing props and added
		// a new prop called update session
		return <LoginUser {...props} updateSession={updateSession} />;
	};

	return (
		<ApolloProvider client={client}>
			<BrowserRouter>
				<NavBar />
				<Switch>
					<Route exact path="/" component={RegisterUser} />
					<Route exact path="/login" render={loggedUser} />
					<Route exact path="/book" component={Room} />
					<Route path="/logout" component={Logout} />

					{localStorage.getItem("role") === "Admin" ? (
						<Switch>
							<Route
								exact
								path="/createroom"
								component={CreateRoom}
							/>
							<Route
								exact
								path="/transactions"
								component={Transactions}
							/>
							<Route
								exact
								path="/room/update/:id"
								component={UpdateRoom}
							/>
						</Switch>
					) : (
						""
					)}
					<Route
						exact
						path="/transaction/:id"
						component={Transaction}
					/>
				</Switch>
				<DemoFooter />
			</BrowserRouter>
		</ApolloProvider>
	);
}

export default App;
