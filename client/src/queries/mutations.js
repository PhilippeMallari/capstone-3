import { gql } from "apollo-boost";

const createUserMutation = gql`
	mutation(
		$username: String!
		$email: String!
		$contact_number: String!
		$password: String!
	) {
		createUser(
			username: $username
			email: $email
			contact_number: $contact_number
			password: $password
		) {
			id
			username
			email
			contact_number
			password
		}
	}
`;

const loginUserMutation = gql`
	mutation($username: String!, $password: String!) {
		loginUser(username: $username, password: $password) {
			id
			username
			password
			role
		}
	}
`;

const createRoomMutation = gql`
	mutation(
		$name: String!
		$description: String!
		$room_type: String!
		$imageLocation: String
		$persons: String!
		$price: String!
		$bed_size: String!
		$status: String!
	) {
		createRoom(
			name: $name
			description: $description
			room_type: $room_type
			imageLocation: $imageLocation
			persons: $persons
			price: $price
			bed_size: $bed_size
			status: $status
		) {
			id
			name
			description
			room_type
			imageLocation
			persons
			price
			bed_size
			status
		}
	}
`;

const deleteRoomMutation = gql`
	mutation($id: String!) {
		deleteRoom(id: $id)
	}
`;

const updateRoomMutation = gql`
	mutation(
		$id: ID!
		$name: String!
		$description: String!
		$room_type: String!
		$imageLocation: String
		$persons: String!
		$price: String!
		$bed_size: String!
		$status: String!
	) {
		updateRoom(
			id: $id
			name: $name
			description: $description
			room_type: $room_type
			imageLocation: $imageLocation
			persons: $persons
			price: $price
			bed_size: $bed_size
			status: $status
		) {
			id
			name
			description
			room_type
			imageLocation
			persons
			price
			bed_size
			status
		}
	}
`;

const createTransactionMutation = gql`
	mutation(
		$room_id: String!
		$user_id: String
		$payment_mode_id: String!
		$total_price: String!
		$check_in: String!
		$check_out: String!
		$status: String!
	) {
		createTransaction(
			room_id: $room_id
			user_id: $user_id
			payment_mode_id: $payment_mode_id
			total_price: $total_price
			check_in: $check_in
			check_out: $check_out
			status: $status
		) {
			room_id {
				name
			}
			user_id {
				username
			}
			payment_mode_id {
				name
			}
			total_price
			check_in
			check_out
			status
		}
	}
`;

export {
	createUserMutation,
	loginUserMutation,
	createRoomMutation,
	deleteRoomMutation,
	updateRoomMutation,
	createTransactionMutation
};
