import { gql } from "apollo-boost";

const getUsersQuery = gql`
	{
		getUsers {
			username
			email
			contact_number
			password
		}
	}
`;

const getRoomsQuery = gql`
	{
		getRooms {
			id
			name
			description
			room_type
			imageLocation
			persons
			price
			bed_size
			status
		}
	}
`;

const getRoomQuery = gql`
	query($id: ID!) {
		getRoom(id: $id) {
			id
			name
			description
			room_type
			imageLocation
			persons
			price
			bed_size
			status
		}
	}
`;

const getPaymentModes = gql`
	{
		getPaymentModes {
			id
			name
		}
	}
`;

const getTransactionsQuery = gql`
	query($user_id: String) {
		getTransactions(user_id: $user_id) {
			id
			room_id {
				name
				description
				room_type
				persons
				bed_size
			}
			user_id {
				username
				email
				contact_number
			}
			payment_mode_id {
				name
			}
			total_price
			check_in
			check_out
			status
		}
	}
`;

const getRoles = gql`
	query {
		getRoles {
			id
			description
		}
	}
`;

export {
	getUsersQuery,
	getRoomsQuery,
	getRoomQuery,
	getPaymentModes,
	getTransactionsQuery,
	getRoles
};
