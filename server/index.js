// Backend Server
const express = require("express");
const mongoose = require("mongoose");
const Room = require("./models/Room.js");
const bodyParser = require("body-parser");

const cors = require("cors")

// instantiate an express project
// serve our project using express
const port = 4000;
const app = express();

//HEROKU
let databaseURL = process.env.DATABASE_URL || "mongodb+srv://philippemallari:Toodope04@cluster0-srijx.mongodb.net/montalban_waterpark?retryWrites=true&w=majority"

// Database Connection
mongoose.connect(databaseURL,
	{
		useCreateIndex: true,
		useNewUrlParser: true
	}
);

// Check if connection succeeds
mongoose.connection.once("open", () => {
	console.log("Now connected to the mongo DB server");
});

app.use(bodyParser.json({ limit: "15mb" }));

// allow the users to access a server in the folder by serving the static data
// syntax: app.use("/path", express.static("folder to serve"))
app.use("/images", express.static("images"));

// allow app to use the cors
app.use(cors())

//import the instantiation of the apollo server
const server = require("./queries/queries.js");

//the app will be served by the apollo server instead of express
server.applyMiddleware({
	app,
	path: "/montalban_waterpark"
});

//HEROKU
let PORT = process.env.PORT || 4000

//server initialization
app.listen(PORT, () => {
	console.log(
		`🚀  Server ready at http://localhost:4000${server.graphqlPath}`
	);
});
