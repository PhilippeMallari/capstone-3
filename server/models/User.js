const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const userSchema = new Schema({
	username: {
		type: String,
		required: true,
		unique: true
	},

	email: {
		type: String,
		required: true
	},

	contact_number: {
		type: String,
		required: true
	},

	password: {
		type: String,
		required: true
	},

	role: {
		type: String,
		default: "User"
	}
});

module.exports = mongoose.model("User", userSchema);
