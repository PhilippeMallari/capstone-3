const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const transactionSchema = new Schema({
	room_id : {
		type: String,
		required: true
	},
	user_id : {
		type: String,
		required: true 
	},
	total_price : {
		type: Number,
		required: true 
	},
	check_in : {
		type : String,
		required: true 
	},
	check_out : {
		type : String,
		required: true 
	},
	status : {
		type: String,
		required: true 
	},
	payment_mode_id : {
		type: String,
		required: true
	}
})

module.exports = mongoose.model("Transaction", transactionSchema);