const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const paymentModeSchema = new Schema({
	name: {
		type: String,
		required: true
	}
});

module.exports = mongoose.model("Payment_Mode", paymentModeSchema);