const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const messageSchema = new Schema({
	description : {
		type: String,
		required: true
	}, 
	email : {
		type: String,
		required: true
	},
	contact_number : {
		type: Number,
		required: true
	}
})

module.exports = mongoose.model("Message", messageSchema);