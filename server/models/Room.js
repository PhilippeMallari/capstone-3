const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const roomSchema = new Schema({
	name: {
		type: String,
		required: true
	},
	description: {
		type: String,
		required: true
	},
	room_type: {
		type: String,
		required: true
	},
	imageLocation: {
		type: String
	},
	persons: {
		type: String,
		required: true
	},
	price: {
		type: String,
		required: true
	},
	bed_size: {
		type: String,
		required: true
	},
	status: {
		type: String,
		required: true
	}
});

module.exports = mongoose.model("Room", roomSchema);
