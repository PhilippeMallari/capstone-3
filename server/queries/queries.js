const { ApolloServer, gql } = require("apollo-server-express");
const { GraphQLDateTime } = require("graphql-iso-date");
const bcrypt = require("bcrypt");

const uuid = require("uuid/v1");
const fs = require("fs");

// mongoose models
const Room = require("../models/Room.js");
const PaymentMode = require("../models/PaymentMode.js");
const Message = require("../models/Message.js");
const Role = require("../models/Role.js");
const User = require("../models/User.js");
const Transaction = require("../models/Transaction.js");

//CRUD
// type Query == Retrieve/Read
// type Mutation == Create/ Update/ Delete
// resolver for Date Schema to be able to handle date data types

const typeDefs = gql`
	# the type query is the root of all GraphQL queries
	# this is used for executing "GET" requests

	type RoomType {
		id: ID
		name: String
		description: String
		room_type: String
		imageLocation: String
		persons: String
		price: String
		bed_size: String
		status: String
	}

	type PaymentModeType {
		id: ID
		name: String
	}

	type MessageType {
		id: ID
		description: String
		email: String
		contact_number: String
	}

	type RoleType {
		id: ID
		description: String
	}

	type UserType {
		id: ID
		username: String
		email: String
		contact_number: String
		password: String
		role: String
	}

	type TransactionType {
		id: ID
		room_id: [RoomType]
		user_id: [UserType]
		payment_mode_id: [PaymentModeType]
		total_price: String
		check_in: String
		check_out: String
		status: String
	}

	type Query {
		getRooms: [RoomType]
		getPaymentModes: [PaymentModeType]
		getMessages: [MessageType]
		getRoles: [RoleType]
		getUsers: [UserType]
		getTransactions(user_id: String): [TransactionType]
		getRoom(id: ID!): RoomType
	}

	#CUD Functionality=====================================================
	#We are mutating the server/database
	type Mutation {
		createRoom(
			name: String
			description: String
			room_type: String
			imageLocation: String
			persons: String
			price: String
			bed_size: String
			status: String
		): RoomType

		createTransaction(
			room_id: String
			user_id: String
			payment_mode_id: String
			total_price: String
			check_in: String
			check_out: String
			status: String
		): TransactionType

		createUser(
			username: String
			email: String
			contact_number: String
			password: String
		): UserType

		#Login Mutation============================================================

		loginUser(username: String!, password: String!): UserType

		#Update Functions==========================================================
		updateRoom(
			id: ID!
			name: String
			description: String
			room_type: String
			imageLocation: String
			persons: String
			price: String
			bed_size: String
			status: String
		): RoomType

		updateTransaction(
			id: ID!
			payment_mode_id: String
			check_in: String
			check_out: String
			status: String
		): TransactionType

		updateUser(
			id: ID!
			username: String
			email: String
			contact_number: Int
			password: String
		): UserType

		deleteRoom(id: String!): Boolean
		deleteTransaction(id: String!): Boolean
		deleteUser(id: String!): Boolean
	}
`;

const resolvers = {
	Query: {
		// getRoom : (id) => {
		// 	return Room.findById(id)
		// },

		getRooms: () => {
			// console.log(Room.find({}));
			return Room.find({});
		},

		getPaymentModes: () => {
			// console.log(PaymentMode.find({}));
			return PaymentMode.find({});
		},

		getMessages: () => {
			// console.log(Message.find({}))
			return Message.find({});
		},

		getRoles: () => {
			// console.log(Role.find({}))
			return Role.find({});
		},

		getUsers: () => {
			// console.log(Role.find({}))
			return User.find({});
		},

		getTransactions: (_, args) => {
			// console.log(Transaction.find({}))
			if (!args) {
				return Transaction.find({});
			} else {
				return Transaction.find({ user_id: args.user_id });
			}
		},

		getRoom: (_, args) => {
			return Room.findById(args.id);
		}
	},

	Mutation: {
		createRoom: (_, args) => {
			console.log(args.imageLocation);

			// assign the variable image string to the value of the encoded file
			let imageString = args.imageLocation;

			// in order to decode the encoded data, we need to remove the text before the encoded string. we need to remove data:/png;base64
			let imageBase = imageString.split(";base64,").pop();

			// console.log("this is the encoded file + " + imageBase);

			// declare the location of the images folder in the server and assign a unique name for our file using uuid

			let imageLocation = "images/" + uuid() + ".png";
			console.log(imageLocation);

			// syntax fs.writeFile(filename/descriptor, data, options, callback function)
			fs.writeFile(
				imageLocation,
				imageBase,
				{ encoding: "base64" },
				err => {}
			);

			let newRoom = Room({
				name: args.name,
				description: args.description,
				room_type: args.room_type,
				imageLocation: imageLocation,
				persons: args.persons,
				price: args.price,
				bed_size: args.bed_size,
				status: args.status
			});

			return newRoom.save();
		},

		createTransaction: (_, args) => {
			let newTransaction = Transaction({
				room_id: args.room_id,
				user_id: args.user_id,
				payment_mode_id: args.payment_mode_id,
				total_price: args.total_price,
				check_in: args.check_in,
				check_out: args.check_out,
				status: args.status
			});

			return newTransaction.save();
		},

		createUser: (_, args) => {
			let newUser = User({
				username: args.username,
				email: args.email,
				contact_number: args.contact_number,
				password: bcrypt.hashSync(args.password, 5)
			});

			return newUser.save();
		},

		loginUser: (_, args) => {
			console.log("Trying to log in");

			return User.findOne({ username: args.username }).then(user => {
				if (user === null) {
					return null;
				}
				// console.log(user.password);
				// console.log(args.password);

				let hashedPassword = bcrypt.compareSync(
					args.password,
					user.password
				);

				if (!hashedPassword) {
					console.log("Wrong password");
					return null;
				} else {
					console.log(user);
					return user;
				}
			});
		},

		updateRoom: (_, args) => {
				// assign the variable image string to the value of the encoded file
			let imageString = args.imageLocation;

			// in order to decode the encoded data, we need to remove the text before the encoded string. we need to remove data:/png;base64
			let imageBase = imageString.split(";base64,").pop();

			// console.log("this is the encoded file + " + imageBase);

			// declare the location of the images folder in the server and assign a unique name for our file using uuid

			let imageLocation = "images/" + uuid() + ".png";
			console.log(imageLocation);

			// syntax fs.writeFile(filename/descriptor, data, options, callback function)
			fs.writeFile(
				imageLocation,
				imageBase,
				{ encoding: "base64" },
				err => {}
			);
			return Room.findByIdAndUpdate(args.id, {
				$set: {
					name: args.name,
					description: args.description,
					room_type: args.room_type,
					imageLocation: imageLocation,
					persons: args.persons,
					price: args.price,
					bed_size: args.bed_size,
					status: args.status
				}
			});
		},

		updateTransaction: (_, args) => {
			return Transaction.findByIdAndUpdate(args.id, {
				$set: {
					payment_mode_id: args.payment_mode_id,
					check_in: args.check_in,
					check_out: args.check_out,
					status: args.status
				}
			});
		},

		updateUser: (_, args) => {
			return User.findByIdAndUpdate(args.id, {
				$set: {
					username: args.username,
					email: args.email,
					contact_number: args.contact_number,
					password: args.password
				}
			});
		},

		deleteRoom: (_, args) => {
			let condition = args.id;

			return Room.findByIdAndDelete(condition).then((room, err) => {
				if (!room) {
					console.log("Delete failed, no room found");
					return false;
				}

				console.log("Room Deleted");
				return true;
			});
		},

		deleteTransaction: (_, args) => {
			let condition = args.id;

			return Transaction.findByIdAndDelete(condition).then(
				(transaction, err) => {
					if (!transaction) {
						console.log("Delete failed, no transaction found");
						return false;
					}

					console.log("Transaction Deleted");
					return true;
				}
			);
		},

		deleteUser: (_, args) => {
			let condition = args.id;

			return User.findByIdAndDelete(condition).then((user, err) => {
				if (!user) {
					console.log("Delete failed no user found");
					return false;
				}

				console.log("User Deleted");
				return true;
			});
		}
	},

	TransactionType: {
		room_id: (parent, args) => {
			return Room.find({ _id: parent.room_id });
		},

		user_id: (parent, args) => {
			return User.find({ _id: parent.user_id });
		},

		payment_mode_id: (parent, args) => {
			return PaymentMode.find({ _id: parent.payment_mode_id });
		}
	}
};

// create an instance of the apollo server
// In the most basic sense, the ApolloServer can be started
// by passing schema type definitions(typeDefs) and the resolvers
// responsible for fetching data for the declared requests/queries
const server = new ApolloServer({
	typeDefs,
	resolvers
});

module.exports = server;
